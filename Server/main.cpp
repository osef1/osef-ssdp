#include "SsdpServer.h"

#include <iostream>

int main()
{
    int ret = -1;

    OSEF::SsdpServer server("eth0", "1919");

    std::string service = "";
    std::string wait = "";
    timespec to = {60, 0};
    if (server.receiveSearchRequest(service, wait, to))
    {
        if (service == OSEF::SsdpSearchTargetAll)
        {
            const OSEF::SsdpSearchResponseFields searchFields =
            {
                "this_is_my_location",
                "this_is_my_server",
                "this_is_my_target",
                "this_is_my_unique_id"
            };

            if (server.sendSearchResponse(searchFields))
            {
                ret = 0;
            }
        }
    }

    return ret;
}
