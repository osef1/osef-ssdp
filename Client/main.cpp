#include "SsdpClient.h"

#include <getopt.h>
#include <iostream>

 bool getInterfaceName(int argc, char** argv, std::string& ifName)
{
    bool ret = true;
    int32_t opt = -1;

    if (argc > 1)
    {
        do
        {
            opt = getopt(argc, argv, "i:");
            if (opt != -1)
            {
                switch (opt)
                {
                    case 'i':
                        ifName.assign(optarg);
                        break;
                    default:
                        std::cout << "Usage:   " << argv[0] << " [-option] [argument]" << std::endl;
                        std::cout << "option:  " << std::endl;
                        std::cout << "         " << "-i  Ethernet interface (wlan0)" << std::endl;
                        ret = false;
                        break;
                }
            }
        }while (ret && (opt != -1));
    }

    return ret;
}

int main(int argc, char** argv)
{
    int ret = -1;

    std::string interface = "wlan0";
    if (getInterfaceName(argc, argv, interface))
    {
        OSEF::SsdpClient client(interface);
        if (client.search())
        {
            OSEF::DI_Container identifications;
            if (client.getIdentifications(identifications))
            {
                for (std::string id : identifications)
                {
                    std::cout << "Device " << id << std::endl;

                    std::string location = "";
                    if (client.getField(id, "LOCATION", location))
                    {
                        std::cout << "    Location " << location << std::endl;
                    }
                    else
                    {
                        std::cout << "    No location" << std::endl;
                    }
                }
                ret = 0;
            }
        }
    }

    return ret;
}
