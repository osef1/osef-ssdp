#!/bin/bash

cd build/coverage

./"$@"

#gcovr . --exclude-directories CMakeFiles --exclude-directories googletest-build -r ../../.. --html --html-details -o index.html
gcovr .  --exclude-directories Test/gtest -r ../../.. --html --html-details -o index.html
