#include "SsdpClient.h"
#include "SsdpSearchRequest.h"
#include "TimeOut.h"
#include "Debug.h"

#include <unordered_map>

OSEF::SsdpClient::SsdpClient(const std::string& lh, const std::string& port)
    :client(SsdpMulticastGroup, port, lh){}

OSEF::SsdpClient::~SsdpClient() {}

bool OSEF::SsdpClient::getField(const std::string& id, const std::string& field, std::string& value)
{
    bool ret = false;

    DD_Fields device;
    if (getDevice(id, device))
    {
        HF_Container::iterator f = device.find(field);
        if (f != device.end())
        {
            value = f->second;
            ret = true;
        }
        else
        {
            DERR("could not find field " << field << " for device " << id);
        }
    }

    return ret;
}

bool OSEF::SsdpClient::getDevice(const std::string& id, DD_Fields& device)
{
    bool ret = false;

    DD_Iterator d = devices.find(id);
    if (d != devices.end())
    {
        device = d->second;
        ret = true;
    }
    else
    {
        DERR("could not find device " << id);
    }

    return ret;
}

bool OSEF::SsdpClient::getIdentifications(DI_Container& identifications)
{
    bool ret = false;

    for (DD_Pair dev : devices)
    {
        identifications.push_back(dev.first);
        ret = true;
    }

    return ret;
}

bool OSEF::SsdpClient::getSplitLocation(const std::string& id, SplitLocation& sploc)
{
    bool ret = false;

    std::string location = "";
    if (getField(id, SsdpLocationName, location))
    {
        ret = splitLocation(location, sploc);
    }

    return ret;
}

 bool OSEF::SsdpClient::splitLocation(const std::string& location, SplitLocation& sploc)
{
    bool ret = false;

    size_t ipStart = location.find("://", 0);
    if ((ipStart != std::string::npos) && (location.size() > (ipStart+3)))
    {
        ipStart += 3;
        size_t portStart = location.find(":", ipStart+3);
        if ((portStart != std::string::npos) && (location.size() > (portStart+1)))
        {
            portStart += 1;
            size_t uriSeparator = location.find("/", portStart);
            if ((uriSeparator != std::string::npos) && (location.size() > (uriSeparator+1)))
            {
                sploc.uri = location.substr(uriSeparator+1, location.size()-(uriSeparator+1));
            }
            else
            {
                uriSeparator = location.size();
                sploc.uri = "";
            }

            sploc.protocol = location.substr(0, ipStart-3);
            sploc.ip = location.substr(ipStart, portStart-ipStart-1);
            sploc.port = location.substr(portStart, uriSeparator-portStart);
            ret = true;
        }
        else
        {
            DERR("error searching port separator");
        }
    }
    else
    {
        DERR("error searching IP separator");
    }

    return ret;
}

bool OSEF::SsdpClient::search(const uint8_t& wait, const std::string& searchTarget, const std::string& idFieldName)
{
    bool ret = false;

    if (sendSearchRequest(wait, searchTarget))
    {
        DOUT("Sent SSDP search request for target " << searchTarget);

        OSEF::TimeOut timer({wait+1, 0});  // Add one extra second to mandatory receiving period
        if (timer.start())
        {
            uint32_t responseCount = 0;

            do
            {
                if (receiveSearchResponse(timer.getRemainingTimeNow(), idFieldName))
                {
                    responseCount++;
                    ret = true;
                }
            }while (not timer.isElapsedNow());

            std::cout << "SSDP client received " << responseCount << " response(s)" << std::endl;
        }
    }

    return ret;
}

bool OSEF::SsdpClient::sendSearchRequest(const uint8_t& wait, const std::string& searchTarget)
{
    bool ret = false;

    OSEF::SsdpSearchRequest request;
    if (request.pushRequest(client.getPort(), wait, searchTarget))
    {
        ret = client.sendString(request.asString());
    }

    return ret;
}

bool OSEF::SsdpClient::receiveSearchResponse(const timespec& to, const std::string& idFieldName)
{
    bool ret = false;

    SsdpSearchResponse response;
    if (client.receiveString(response.getInnerString(), response.getBufferSize(), to))
    {
        if (response.resetForExtraction(response.asString().size()))
        {
            if (response.extractResponse())
            {
                ret = storeSearchResponse(response, idFieldName);
            }
        }
    }

    return ret;
}

bool OSEF::SsdpClient::storeSearchResponse(const SsdpSearchResponse& response, const std::string& idFieldName)
{
    bool ret = false;

    std::string deviceId = "";
    if (response.getField(idFieldName, deviceId))
    {
        DD_Iterator device = devices.find(deviceId);
        if (device == devices.end())
        {
            if (devices.insert({deviceId, response.getHeaderFields()}).second)
            {
                ret = true;
                DOUT("SSDP client found device " << deviceId);
            }
            else
            {
                DERR("error inserting " << deviceId << " into devices container");
            }
        }
        else
        {
            device->second = response.getHeaderFields();
            ret = true;
            DOUT("SSDP client updated " << deviceId << " device");
        }
    }
    else
    {
        DERR("error header fields without id field named " << idFieldName);
    }

    return ret;
}
