#ifndef OSEFSSDPSEARCHRESPONSE_H
#define OSEFSSDPSEARCHRESPONSE_H

#include "HttpFrame.h"
#include "Ssdp.h"

#include <string>

namespace OSEF
{
    // SSDP search response fields
    typedef struct
    {
        std::string location;
        std::string server;
        std::string searchTarget;
        std::string uuid;
    }SsdpSearchResponseFields;

    class SsdpSearchResponse : public HttpFrame
    {
    public:
        SsdpSearchResponse();
        virtual ~SsdpSearchResponse();

        bool extractResponse();
        bool pushResponse(const SsdpSearchResponseFields& searchFields, const uint32_t& maxAge = 1800);

        std::string getLocation(const std::string& ip, const std::string& port, const std::string& uri) const;
        std::string getServer(const std::string& name, const std::string& version) const;

    private:
        SsdpSearchResponse(const SsdpSearchResponse& orig);
        SsdpSearchResponse& operator=(const SsdpSearchResponse& orig);
    };
}  // namespace OSEF

#endif /* OSEFSSDPSEARCHRESPONSE_H */
