#include "SsdpSearchResponse.h"
#include "Debug.h"

#include <sys/utsname.h>
#include <string>

OSEF::SsdpSearchResponse::SsdpSearchResponse() {}

OSEF::SsdpSearchResponse::~SsdpSearchResponse() {}

bool OSEF::SsdpSearchResponse::extractResponse()
{
    const bool ret = extractResponseHeader();
    return ret;
}

// LOCATION URL to UPnP description of root device
// CACHE-CONTROL “max-age=[>=1800]" in seconds
// EXT  no value, just for UPnP1.0 compat ...
// SERVER OS name/OS version UPnP/2.0 product name/product version (i.e. unix/5.1 UPnP/2.0 MyProduct/1.0)
// ST
// USN unique service name uuid:igd73616d61-6a65-7374-650a-3427923d3b10::upnp:rootdevice
// BOOTID.UPNP.ORG

bool OSEF::SsdpSearchResponse::pushResponse(const SsdpSearchResponseFields& searchFields, const uint32_t& maxAge)
{
    bool ret = false;

    if (maxAge >= 1800)
    {
        HF_Container fields
        (
            {
                {"LOCATION", searchFields.location},
                {"SERVER", searchFields.server},
                {"ST", searchFields.searchTarget},
                {"USN", "uuid:" + searchFields.uuid},
                {"CACHE-CONTROL", std::to_string(maxAge)},
                {"EXT", ""},
                {"BOOTID.UPNP.ORG", "0"}
            }
        );

        ret = pushHeader(HttpStatusOK, fields);
    }
    else
    {
        DERR("Max age " << maxAge << " smaller than 1800");
    }

    return ret;
}

std::string OSEF::SsdpSearchResponse::getLocation(const std::string& ip, const std::string& port, const std::string& uri) const
{
    return "http://" + ip + ":" + port + "/" + uri;
}

// unix/5.1 UPnP/2.0 MyProduct/1.0
// Linux/2.6 UPnP/1.0 fbxigdd/1.1
// Linux/4.1.20-1.9pre-02982-g3239e93ffc4f, UPnP/1.0, Portable SDK for UPnP devices/1.6.18
std::string OSEF::SsdpSearchResponse::getServer(const std::string& name, const std::string& version) const
{
    std::string ret = "";

    struct utsname os;
    if (uname(&os) == 0)
    {
        ret += std::string(os.sysname) + "/" + std::string(os.release) + " UPnP/2.0 " + name + "/" + version;
    }

    return ret;
}
