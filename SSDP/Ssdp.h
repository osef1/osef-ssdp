#ifndef OSEFSSDP_H
#define OSEFSSDP_H

#include <string>
#include <unordered_map>

namespace OSEF
{
    const std::string SsdpMulticastGroup = "239.255.255.250";
    const std::string SsdpMulticastPort = "1900";

    const std::string SsdpSearchRequestLine = "M-SEARCH * HTTP/1.1";

    const std::string SsdpHostName = "HOST";
    const std::string SsdpMechanismName = "MAN";
    const std::string SsdpMechanismValue = "\"ssdp:discover\"";
    const std::string SsdpSearchTargetName = "ST";
    const std::string SsdpSearchTargetAll = "ssdp:all";
    const std::string SsdpMaxWaitName = "MX";

    const std::string SsdpUniqueServiceNameName = "USN";
    const std::string SsdpLocationName = "LOCATION";
}  // namespace OSEF

#endif /* OSEFSSDP_H */
