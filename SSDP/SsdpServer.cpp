#include "SsdpServer.h"
#include "SsdpSearchRequest.h"
#include "SsdpSearchResponse.h"
#include "Debug.h"

OSEF::SsdpServer::SsdpServer(const std::string& lh, const std::string& p)
    :server(SsdpMulticastGroup, p, lh){}

OSEF::SsdpServer::~SsdpServer() {}

bool OSEF::SsdpServer::receiveSearchRequest(std::string& searchTarget, std::string& wait, const timespec& to)
{
    bool ret = false;

    SsdpSearchRequest request;
    if (server.receiveString(request.getInnerString(), request.getBufferSize(), to))
    {
        if (request.resetForExtraction(request.asString().size()))
        {
            ret = request.extractRequest(searchTarget, wait, server.getPort());
        }
    }

    return ret;
}

bool OSEF::SsdpServer::sendSearchResponse(const SsdpSearchResponseFields& searchFields, const uint32_t& maxAge)
{
    bool ret = false;

    SsdpSearchResponse response;
    if (response.pushResponse(searchFields, maxAge))
    {
        ret = server.sendString(response.asString());
    }

    return ret;
}
