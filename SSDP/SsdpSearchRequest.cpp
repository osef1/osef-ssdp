#include "SsdpSearchRequest.h"
#include "Debug.h"

OSEF::SsdpSearchRequest::SsdpSearchRequest(){}

OSEF::SsdpSearchRequest::~SsdpSearchRequest() {}

// https://openconnectivity.org/upnp-specs/UPnP-arch-DeviceArchitecture-v2.0-20200417.pdf

// M-SEARCH * HTTP/1.1
// HOST: 239.255.255.250:1900
// MAN: "ssdp:discover"
// MX: seconds to delay response
// ST: search target

bool OSEF::SsdpSearchRequest::pushRequest(const std::string& port, const uint32_t& wait, const std::string& searchTarget)
{
    bool ret = false;

    if ((wait >= 1) && (wait <= 5))
    {
        HF_Container fields
        (
            {
                {SsdpHostName, getHostValue(port)},
                {SsdpMechanismName, SsdpMechanismValue},
                {SsdpMaxWaitName, std::to_string(wait)},
                {SsdpSearchTargetName, searchTarget}
            }
        );

        ret = pushHeader(SsdpSearchRequestLine, fields);
    }
    else
    {
        DERR("Wait time " << wait << " out of bounds [1-5]");
    }

    return ret;
}

bool OSEF::SsdpSearchRequest::extractRequest(std::string& searchTarget, std::string& wait, const std::string& port)
{
    bool ret = false;

    if (extractHeaderStartLine(SsdpSearchRequestLine))
    {
        if (isFieldEqual(SsdpHostName, getHostValue(port)))
        {
            if (isFieldEqual(SsdpMechanismName, SsdpMechanismValue))
            {
                if (getField(SsdpSearchTargetName, searchTarget))
                {
                    ret = getField(SsdpMaxWaitName, wait);
                }
            }
        }
    }

    return ret;
}
