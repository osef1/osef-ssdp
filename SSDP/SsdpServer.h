#ifndef OSEFSSDPSERVER_H
#define OSEFSSDPSERVER_H

#include "HttpFrame.h"
#include "Ssdp.h"
#include "UdpMulticastServer.h"
#include "SsdpSearchResponse.h"

#include <string>

namespace OSEF
{
    class SsdpServer
    {
    public:
        explicit SsdpServer(const std::string& lh, const std::string& p = SsdpMulticastPort);
        virtual ~SsdpServer();

        bool receiveSearchRequest(std::string& searchTarget, std::string& wait, const timespec& to);
        bool sendSearchResponse(const SsdpSearchResponseFields& searchFields, const uint32_t& maxAge = 1800);

    private:
        SsdpServer(const SsdpServer& orig);
        SsdpServer& operator=(const SsdpServer& orig);

        UdpMulticastServer server;
    };
}  // namespace OSEF

#endif /* OSEFSSDPSERVER_H */
