#ifndef OSEFSSDPSEARCHREQUEST_H
#define OSEFSSDPSEARCHREQUEST_H

#include "HttpFrame.h"
#include "Ssdp.h"

#include <string>

namespace OSEF
{
    class SsdpSearchRequest : public HttpFrame
    {
    public:
        SsdpSearchRequest();
        virtual ~SsdpSearchRequest();

        bool pushRequest(const std::string& port = SsdpMulticastPort, const uint32_t& wait = 1, const std::string& searchTarget = SsdpSearchTargetAll);
        bool extractRequest(std::string& searchTarget, std::string& wait, const std::string& port = SsdpMulticastPort);

    private:
        SsdpSearchRequest(const SsdpSearchRequest& orig);
        SsdpSearchRequest& operator=(const SsdpSearchRequest& orig);

        const std::string getHostValue(const std::string& port) const {return SsdpMulticastGroup + ":" + port;}
    };
}  // namespace OSEF

#endif /* OSEFSSDPSEARCHREQUEST_H */
