#ifndef SSDPCLIENT_H
#define SSDPCLIENT_H

#include "Ssdp.h"
#include "UdpMulticastClient.h"
#include "SsdpSearchResponse.h"  // HF_Container

#include <list>
#include <string>
#include <unordered_map>
#include <utility>  // pair

namespace OSEF
{
    // Discovered Devices
    typedef std::string DD_Identification;
    typedef HF_Container DD_Fields;
    typedef std::pair<DD_Identification, DD_Fields> DD_Pair;
    typedef std::unordered_map<DD_Identification, DD_Fields> DD_Container;
    typedef DD_Container::iterator DD_Iterator;

    // Split Locations
    typedef struct
    {
        std::string protocol;
        std::string ip;
        std::string port;
        std::string uri;
    }SplitLocation;

//    typedef SplitLocation SL_Element;
//    typedef std::list<SL_Element> SL_Container;

    //  Devices Identification
    typedef std::list<DD_Identification> DI_Container;

    class SsdpClient
    {
    public:
        explicit SsdpClient(const std::string& lh, const std::string& port = SsdpMulticastPort);
        virtual ~SsdpClient();

        bool search(const uint8_t& wait = 1, const std::string& searchTarget = SsdpSearchTargetAll, const std::string& idFieldName = SsdpUniqueServiceNameName);

        bool getField(const std::string& id, const std::string& field, std::string& value);
        bool getDevice(const std::string& id, DD_Fields& device);
        bool getIdentifications(DI_Container& identifications);

        bool getSplitLocation(const std::string& id, SplitLocation& sploc);

    private:
        SsdpClient(const SsdpClient& orig);
        SsdpClient& operator=(const SsdpClient& orig);

        UdpMulticastClient client;
        DD_Container devices;

        bool sendSearchRequest(const uint8_t& wait, const std::string& searchTarget);
        bool receiveSearchResponse(const timespec& to, const std::string& idFieldName);
        bool storeSearchResponse(const SsdpSearchResponse& response, const std::string& idFieldName);

        bool splitLocation(const std::string& location, SplitLocation& sploc);
    };
}  // namespace OSEF

#endif /* SSDPCLIENT_H */
