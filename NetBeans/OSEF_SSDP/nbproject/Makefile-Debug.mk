#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/6e70608c/SsdpClient.o \
	${OBJECTDIR}/_ext/6e70608c/SsdpSearchRequest.o \
	${OBJECTDIR}/_ext/6e70608c/SsdpSearchResponse.o \
	${OBJECTDIR}/_ext/6e70608c/SsdpServer.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-DDEBUG -Wall -Wextra -Werror
CXXFLAGS=-DDEBUG -Wall -Wextra -Werror

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_ssdp.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_ssdp.a: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_ssdp.a
	${AR} -rv ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_ssdp.a ${OBJECTFILES} 
	$(RANLIB) ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/libosef_ssdp.a

${OBJECTDIR}/_ext/6e70608c/SsdpClient.o: ../../SSDP/SsdpClient.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e70608c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e70608c/SsdpClient.o ../../SSDP/SsdpClient.cpp

${OBJECTDIR}/_ext/6e70608c/SsdpSearchRequest.o: ../../SSDP/SsdpSearchRequest.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e70608c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e70608c/SsdpSearchRequest.o ../../SSDP/SsdpSearchRequest.cpp

${OBJECTDIR}/_ext/6e70608c/SsdpSearchResponse.o: ../../SSDP/SsdpSearchResponse.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e70608c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e70608c/SsdpSearchResponse.o ../../SSDP/SsdpSearchResponse.cpp

${OBJECTDIR}/_ext/6e70608c/SsdpServer.o: ../../SSDP/SsdpServer.cpp
	${MKDIR} -p ${OBJECTDIR}/_ext/6e70608c
	${RM} "$@.d"
	$(COMPILE.cc) -g -I../../osef-http/HTTP -I../../osef-http/osef-netbuffer/NetBuffer -I../../osef-http/osef-netbuffer/osef-posix/osef-log/Debug -I../../osef-http/osef-netbuffer/osef-posix/Socket -I../../osef-http/osef-netbuffer/osef-posix/Time -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/6e70608c/SsdpServer.o ../../SSDP/SsdpServer.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
